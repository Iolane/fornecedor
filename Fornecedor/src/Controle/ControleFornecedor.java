package Controle;

import java.util.ArrayList;
import modelo.*;

public class ControleFornecedor {
    
    private ArrayList<PessoaFisica> listaPessoaFisica;
    private ArrayList<PessoaJuridica> listaPessoaJuridica;
    private ArrayList<Endereco> listaEndereco;
    private ArrayList<Fornecedores> listaFornecedor;
    
    public ControleFornecedor(){
        listaPessoaFisica = new ArrayList<PessoaFisica>();
        listaPessoaJuridica = new ArrayList<PessoaJuridica>();
        listaEndereco = new ArrayList<Endereco>();
        listaFornecedor = new ArrayList<Fornecedores>();
    }
    
    public void adicionarPessoaFisica(PessoaFisica umFornecedor){
        listaPessoaFisica.add(umFornecedor);
    }
    
    public void adicionarPessoaJuridica(PessoaJuridica umFornecedor){
        listaPessoaJuridica.add(umFornecedor);
    }
    
     public void removerPessoaFisica(PessoaFisica umFornecedor){
        listaPessoaFisica.remove(umFornecedor);
    }
     
     public void removerPessoaJuridica(PessoaJuridica umFornecedor){
        listaPessoaJuridica.remove(umFornecedor);
    }
     
     //pesquisa fornecedor por cpf
     public PessoaFisica perquisarPessoaFisica(String Cpf){
        for (PessoaFisica umFornecedor : listaPessoaFisica) {
            if (umFornecedor.getCpf().equalsIgnoreCase(Cpf))
                return umFornecedor;
        }
        return null;
    }
     
     //pesquisa fornecedor por cnpj
     public PessoaJuridica perquisarPessoaJuridica(String Cnpj){
        for (PessoaJuridica umFornecedor : listaPessoaJuridica) {
            if (umFornecedor.getCnpj().equalsIgnoreCase(Cnpj))
                return umFornecedor;
        }
        return null;
    }
    
     public void adicionarEndereco(Endereco umEndereco){
        listaEndereco.add(umEndereco);
    }
     
     /*
     
     public Fornecedores perquisarFornecedor(String Cpf){
        for (Fornecedores (PessoaFisica)umFornecedor : listaFornecedor){
            if ((PessoaFisica)umFornecedor.getCpf().equalsIgnoreCase(Cpf))
                return (PessoaFisica)umFornecedor;
        }
        return null;
    }
    
     public void removerEndereco(Endereco umEndereco){
        listaEndereco.remove(umEndereco);
    }
    //Pesquisa endereço por BAIRRO.
    public Endereco pesquisarEndereco(String umBairro) {
            for (Endereco local: listaEndereco) {
                if (local.getBairro().equalsIgnoreCase(umBairro)) return local;
            }
            return null;
        } 
     */
    
}
