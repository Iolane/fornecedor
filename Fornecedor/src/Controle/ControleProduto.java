package Controle;

import java.util.ArrayList;
import modelo.Produtos;

public class ControleProduto {
    
    private ArrayList<Produtos> listaProdutos;
    
    public ControleProduto(){
        listaProdutos = new ArrayList<Produtos>();
    }
    
    public String adicionarProduto(Produtos umProduto){
        String mensagem = "Adicionado com Sucesso!";
        listaProdutos.add(umProduto);
        return mensagem;
    }
    
    public String removerProduto(Produtos umProduto){
        String mensagem = "Removido com Sucesso!";
        listaProdutos.remove(umProduto);
        return mensagem;
    }
    
    public Produtos perquisarNomeProduto(String umNomeProduto){
        for (Produtos umProduto:listaProdutos) {
            if (umProduto.getNomeProduto().equalsIgnoreCase(umNomeProduto))
                return umProduto;
        }
        return null;
    }
    
}
    
