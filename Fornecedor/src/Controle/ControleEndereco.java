

package Controle;

import java.util.ArrayList;
import modelo.Endereco;

public class ControleEndereco {
    
    private ArrayList<Endereco> listaEndereco;
    
    public ControleEndereco(){
        listaEndereco = new ArrayList<Endereco>();
    }
    
     public String adicionarEndereco(Endereco umEndereco){
        String mensagem = "Adicionado com Sucesso!";
        listaEndereco.add(umEndereco);
        return mensagem;
    }
    
    public String removerEndereco(Endereco umEndereco){
        String mensagem = "Removido com Sucesso!";
        listaEndereco.remove(umEndereco);
        return mensagem;
    }
    //Pesquisa endereço por BAIRRO.
    public Endereco pesquisarEndereco(String umBairro) {
            for (Endereco local: listaEndereco) {
                if (local.getBairro().equalsIgnoreCase(umBairro)) return local;
            }
            return null;
        }    
    
}
