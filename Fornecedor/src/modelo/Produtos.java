package modelo;


public class Produtos {
    
    private String nomeProduto;
    private String quantidade;
    private String preco;
    
    public Produtos(String nomeProduto, String quantidade, String preco){
        this.nomeProduto = nomeProduto;
        this.quantidade = quantidade;
        this.preco = preco;
    }

    public Produtos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void setNomeProduto(String nomeProduto){
        this.nomeProduto = nomeProduto;
    }
    
    public String getNomeProduto(){
        return nomeProduto;
    }
    
    public void setquantidade(String quantidade ){
        this.quantidade = quantidade;
    }
    
    public String getQuantidade(){
        return quantidade;
    }
    
    public void setPreco(String preco){
        this.preco = preco ;
    }
    
    public String getPreco(){
        return preco;
    }
   
}
