
package modelo;

public class Endereco {
    
    private String endereço;
    private String bairro;
     private String cidade;
    private String estado;
    private String cep;
    private String complemento;
    
    public Endereco(String rua, String quadra, String lote, String bairro, String cep){
        this.endereço = rua;
        this.cidade = quadra;
        this.estado = lote;
        this.bairro = bairro;
        this.cep = cep;
    }
    
    public void setEndereco(String rua){
        this.endereço = rua;
    }
    
    public String getEndereco(){
        return endereço;
    }
    
     public void setCidade(String quadra){
        this.cidade = quadra;
    }
    
    public String getCidade(){
        return cidade;
    }
    
     public void setEstado(String lote){
        this.estado = lote ;
    }
    
    public String getEstado(){
        return estado;
    }
    
     public void setBairro(String bairro){
        this.bairro = bairro;
    }
    
    public String getBairro(){
        return bairro;
    }
    
     public void setCep(String cep){
        this.cep = cep;
    }
    
    public String getCep(){
        return cep;
    }
    
     public void setComplemento(String complemento){
        this.complemento = complemento;
    }
    
    public String getComplemento(){
        return complemento;
    }
    
}
