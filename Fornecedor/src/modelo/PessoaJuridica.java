package modelo;



public class PessoaJuridica extends Fornecedores{
    
    private String cnpj;
    private String razaoSocial; 

    public PessoaJuridica(String nome, String telefone, String email, Endereco endereco, String cnpj, String razaoSocial){
        super(nome, telefone, email, endereco);
        this.cnpj = cnpj;
        this.razaoSocial = razaoSocial;

    }

    public void setCnpj(String cnpj){
        this.cnpj = cnpj;
    }

    public String getCnpj(){
        return cnpj;
    }

    public void setRazaoSocial(String razaoSocial){
        this.razaoSocial = razaoSocial;
    }

    public String getRazaoSocial(){
        return razaoSocial;
    }

}
