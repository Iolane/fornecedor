package modelo;

public class Fornecedores {

    protected String nome;
    protected String telefone;
    protected String email;
    protected Endereco endereco;
    
    public Fornecedores(String nome, String telefone, String email, Endereco endereco){
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
        this.endereco = endereco;
    }
   
    
    public void setNomeFornecedor(String nome){
        this.nome = nome;
    }

    public String getNomeFornecedor(){
        return nome;
    }

    public void setTelefoneFornecedor(String telefone){
        this.telefone = telefone;
    }

    public String getTelefoneFoernecedor(){
        return telefone;
    }
    
    public void setEmail(String email){
        this.email = email;
    }
    
    public String getEmail(){
        return email;
    }
    
    public void setEndereco(Endereco endereco){
        this.endereco = endereco;
    }
    
    public Endereco getEndereco(){
        return endereco;
    }

}

