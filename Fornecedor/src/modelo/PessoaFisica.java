
package modelo;

import java.util.ArrayList;

public class PessoaFisica extends Fornecedores {
    
    private String cpf;

    
    public PessoaFisica(String nome, String telefone, String email, Endereco endereco, String cpf){
        super(nome, telefone, email, endereco);
        this.cpf = cpf;  

    }   

    public void setCpf(String umCpf){
        cpf = umCpf;
    }

    public String getCpf(){
        return cpf;
    }
    
}
